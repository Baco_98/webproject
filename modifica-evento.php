<?php
require_once("bootstrap.php");

$evento = $dbh->getEventById($_GET['id'])[0];

if(isset($_POST["categoria"])){
    if(empty($_POST["titolo"]) || empty($_POST["descrizione"]) || empty($_POST["data"]) || empty($_POST["ora"]) || empty($_POST["posti"])  || empty($_POST["prezzo"])  || empty($_POST["citta"])  || empty($_POST["indirizzo"])){
        $templateParams["erroreremodifica"] = "Errore nella modifica. Inserire tutte le informazioni.";
    }
    else{
        //controllo se almeno un campo è stato modificato
        if((!isset($_FILES['img-loader']) || !is_uploaded_file($_FILES['img-loader']['tmp_name'])) && $evento["Titolo"]==$_POST["titolo"] && $evento["Città"]==$_POST["citta"] && $evento["Indirizzo"]==$_POST["indirizzo"] && $evento["Data"]==$_POST["data"] && substr($evento["Ora"], 0, 5)==$_POST["ora"] && $evento["idCategoria"]==$_POST["categoria"] && $evento["Descrizione"]==$_POST["descrizione"] && $evento["NumeroPosti"]==$_POST["posti"] && $evento["Prezzo"]==$_POST["prezzo"]){
            $templateParams["erroreremodifica"] = "Errore nella modifica. Cambiare almeno una informazione.";
        }
        else{
            $place = $dbh->checkPlace($_POST["citta"],$_POST["indirizzo"]);
            if (empty($place[0])){ //controllo se città e indirizzo sono già presenti nel database
                $dbh->insertPlace($_POST["citta"],$_POST["indirizzo"]); //se non ci sono le inserisco
                $place = $dbh->checkPlace($_POST["citta"],$_POST["indirizzo"])[0]["idluogo"];
            }
            else{
                $place = $place[0]["idluogo"];
            }
    
            $uploaddir = 'Immagini eventi/';
            $img = $evento["Immagine"];
    
            if(is_uploaded_file($_FILES['img-loader']['tmp_name'])){ //controllo se è stata cambiata l'immagine di default
                $img_tmp = $_FILES['img-loader']['tmp_name'];
                $img_name = $_FILES['img-loader']['name'];
                while (file_exists($uploaddir . $img_name)) { //controllo se un esiste già un file con lo stesso nome nella cartella di destinazione
                    $reverse = explode('.', strrev($img_name), 2);
                    $img_name = strrev($reverse[1]) . '1.' . strrev($reverse[0]); //aggiungo degli "1" finali se esiste già un file con lo stesso nome
                }
                if (!move_uploaded_file($img_tmp, $uploaddir . $img_name)) { //salvo l'immagine e controllo se si è verificato un errore
                    $templateParams["erroreremodifica"] = "Errore nel caricamento dell'immagine.";
                }else{
                    $img = $uploaddir . $img_name;
                    $templateParams["modifica"] = "Il tuo evento è stato correttamente modicifato.";
                }
            }
            else{
                $templateParams["modifica"] = "Il tuo evento è stato correttamente modificato.";
            }
            $dbh->modifytEvent($_POST["titolo"],$_POST["descrizione"],$_POST["data"],$_POST["ora"],$_POST["posti"],$_POST["prezzo"],$img,$_SESSION["idutente"],$place,$_POST["categoria"],$_GET['id']);
            $utenti = $dbh->getUserIdByEvent($_GET['id']);
            $titolo = "Ops, c'è stato un cambiamento!";
            $testo = "L'evento " . '"' . $evento["Titolo"] . '" a cui parteciperai è stato modificato. Controlla i cambiamenti!';
            foreach ($utenti as $id) {
                $dbh->notifyUser($titolo,$testo,date("Y/m/d"),$id,$_GET['id']);
            }
        }
    }
}

$templateParams["titolo"] = "EEVEEnts - Modifica evento";
$templateParams["nome"] = "modifica-evento-form.php";
$evento = $dbh->getEventById($_GET['id'])[0];

require("template/base.php");
?>