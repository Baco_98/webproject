<div class="container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-12 col-md-10">
            <h1 id="titolo-home" class="text-center mt-4">Benvenuto, trova l'evento più adatto a te e divertiti con noi!</h1>
            <form action="search.php" method="get">
                <label for="ricerca" class="mt-4">Inserisci la città dove vuoi divertirti!</label>
                <div class="input-group mb-5">
                    <input type="text" class="form-control" name="citta" placeholder="Voglio divertirmi a..." id="ricerca">
                    <div class="input-group-btn">
                        <button class="btn btn-light" type="submit"><span class="fa fa-search"></span>Cerca</button>
                    </div>
                </div>
            </form>
            <?php require "template/EventoLayout.php" ?>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>