<div class="container-fluid">
    <div class="row pt-3">
        <div class="col-1 col-md-1"></div>
        <div class="col-11 col-md-10 row">
            <form class="row" action="#" method="POST" enctype="multipart/form-data">
                <div class="col-12 text-center mt-2 mb-3">
                    <h1>Crea il tuo evento!</h1>
                </div>
                <?php if(isset($templateParams["errorerecreazione"])): ?>
                    <div class="col-12 text-danger">
                        <p class="h5 py-2 mb-2 text-center"><?php echo $templateParams["errorerecreazione"]; ?></p>
                    </div>
                <?php endif; ?>
                <?php if(isset($templateParams["creazione"])): ?>
                    <div class="col-12 text-success">
                        <p class="h5 py-3 text-center"><?php echo $templateParams["creazione"]; ?></p>
                    </div>
                <?php endif; ?>
                <div class="form-group col-12 col-md-6 mt-4">
                    <div class="text-center" >
                        <img src="Immagini eventi/default_event.jpg" alt="user image" style="width:250px;height:250px;" id="img">
                    </div>
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="custom-file pb-5 mt-3 col-8">
                            <input type="file" class="custom-file-input" id="img-loader" accept="image/*" name="img-loader">
                            <label class="custom-file-label" for="img-loader">Cambia immagine</label>
                        </div>
                        <div class="col-2"></div>
                    </div>
                    <script>
                    $('#img-loader').change( function(event) {
                        var tmppath = URL.createObjectURL(event.target.files[0]);
                        $("#img").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
                        var fileName = $(this).val().split("\\").pop();
                        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                    });
                    </script>
                </div>
                <div class="form-group col-12 col-md-6 row pt-4">
                    <div class="col-12 row my-2">
                        <label class="col-4" for="titolo" name="titolo">Titolo</label>
                        <input type="text" class="col-8" maxlength="50" name="titolo" id="titolo" placeholder="Inserisci il titolo">
                    </div>
                    <div class="col-12 row my-2">
                        <label class="col-4" for="citta">Città</label>
                        <input type="text" class="col-8" name="citta" maxlength="30" id="citta" placeholder="Inserisci la città">
                    </div>
                    <div class="col-12 row my-2">
                        <label class="col-4" for="indirizzo">Indirizzo</label>
                        <input type="text" class="col-8" name="indirizzo" maxlength="50" id="indirizzo" placeholder="Inserisci l'indirizzo">
                    </div>
                    <div class="col-12 row my-2">
                        <label class="col-4" for="data">Data</label>
                        <input type="date" class="col-8" name="data" id="data" min="<?php echo date("Y-m-d"); ?>">
                        <style>
                            input[type=date]::-webkit-inner-spin-button, input[type=date]::-webkit-outer-spin-button {
                                -webkit-appearance: none;
                            }
                        </style>
                    </div>
                    <div class="col-12 row my-2">
                        <label class="col-4" for="ora">Ora</label>
                        <input type="time" class="col-8" name="ora" id="ora">
                    </div>
                    <div class="col-12 row my-2">
                        <label class="col-4" for="categoria"> Categoria</label>
                        <select class=col-8 id="categoria" name="categoria">
                            <?php
                                $cat = $dbh->getCategories();
                                foreach ($cat as $value) {
                                    echo '<option value="' . $value['idCategoria'] . '">' . $value['nome'] . '</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group col-12 col-md-6 mt-4">
                    <label class="col-12 pt-2" for="descrizione">Descirizione:</label>
                    <textarea class="col-12 pt-3 mt-2" name="descrizione" id="descrizione" style="resize:none;" maxlength="100" placeholder="Inserisci una breve descrizione dell'evento"></textarea>   
                </div>
                <div class="form-group col-12 col-md-6 mt-4">
                    <div class="col-12 my-4 row">
                        <div class="col-md-1"></div>
                        <label class="col-4" for="numPosti">Numero posti:</label>
                        <input type="number" class="col-8 col-md-5" name="posti" id="numPosti" min="1"  onkeydown="return event.keyCode !== 69 && event.keyCode !== 101" onkeypress="return event.charCode != 44 && event.charCode != 45 && event.charCode != 46">
                        <div class="col-md-1"></div>
                    </div>
                    <div class="col-12 my-4 row">
                        <div class="col-md-1"></div>
                        <label class="col-4" for="prezzo">Prezzo biglietto:</label>
                        <input type="number" class="col-8 col-md-5" name="prezzo" id="prezzo" min="0.01" onkeydown="return event.keyCode !== 69 && event.keyCode !== 101" onkeypress="return event.charCode != 46 && event.charCode != 45" step="0.01">
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="form-group text-center col-12 mt-4">
                    <button type="submit" class="btn btn-primary">Crea evento</button>
                </div>
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>