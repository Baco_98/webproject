<!DOCTYPE html>
<html lang="it">
    <head>
        <title><?php echo $templateParams["titolo"]; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
        <!--Libreria per le icone-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script>const x = "<?php echo isUserLoggedIn(); ?>";</script>
        <?php
        if(isset($templateParams["js"])):
            foreach($templateParams["js"] as $script):
        ?>
            <script src="<?php echo $script; ?>"></script>
        <?php
            endforeach;
        endif;
        ?>
        <script src="js/badge.js"></script>
    </head>
    <body style="background-color:WhiteSmoke;">
        <div class="container-fluid">
            <header>
                <svg width="100%" height="10">
                    <rect width="100%" height="10" style="fill:silver;stroke-width:5;stroke:black" />
                </svg>
                <div class="row">
                    <div class="col-7">
                        <a href="index.php">
                            <h1 class="text-center mt-1 float-left pr-2">EEVEEnts</h1>
                            <img src="Immagini/eevee.png" alt="logo eevee" id="eevee">
                        </a>
                    </div>
                    <?php if(!isset($_SESSION["idutente"])): ?>
                        <div class="text-right col-5 my-auto">
                            <button onclick="window.location.href='login.php'" id="accedi">Accedi</button>
                            <button onclick="window.location.href='registrazione.php'" id="iscriviti">Iscriviti</button>
                        </div>
                    <?php endif; ?>
                    <?php if(isset($_SESSION["idutente"])): ?>
                        <div class="col-5 row">
                            <div class="col-sm-4 col-md-6 col-xl-8"></div>
                            <div class="dropdown d-inline col-4 col-sm-2 col-md-2 col-xl-1">
                                <a href="#" data-toggle="dropdown"><img src="<?php echo $_SESSION["immagine"] ?>" alt="menu with possible user action" id="user"></a>
                                <div class="dropdown-menu" id="menu">
                                    <a class="dropdown-item" href="profilo.php">Modifica profilo</a>
                                    <a class="dropdown-item" href="biglietti.php">Biglietti acquistati</a>
                                    <?php if($_SESSION["tipo"] != "cliente"): ?>
                                        <a class="dropdown-item" href="crea-evento.php">Crea un evento</a>
                                        <a class="dropdown-item" href="tuoi-eventi.php">
                                            <?php
                                                if($_SESSION["tipo"] != "amministratore"){
                                                    echo "I tuoi eventi";
                                                }
                                                else{
                                                    echo "Tutti gli eventi";
                                                }
                                            ?>
                                        </a>
                                    <?php endif; ?>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="login.php">Logout</a>
                                </div>
                            </div>
                            <a href="notifiche.php" id="notifiche" class="col-4 col-sm-2 col-md-2 col-xl-1 mx-sm-1 mx-md-0 mx-xl-3">
                                <img src="Immagini/notification.png" alt="notification bell" id="bell">
                                <?php
                                    $num = 0;
                                    if($templateParams["nome"] != "notifiche-layout.php"){
                                        $num = $dbh->getNumberOfNotification($_SESSION["idutente"])[0]["numero"];
                                    }
                                    if($num>0){
                                        echo '<span style="position: absolute;
                                        top: 0px;
                                        right: 50%;
                                        margin-top:15%;
                                        border-radius: 100%;
                                        background-color: red;
                                        color: white;" class="badge">' . $num . '</span>';
                                    }
                                ?>
                            </a>
                            <a href="carrello.php" class="d-inline col-4 col-sm-2 col-md-2 col-xl-1">
                                <img src="Immagini/smart-cart.png" alt="ticket cart" id="cart">
                                <?php if(isUserLoggedIn()): ?>
                                <span style="position: absolute;
                                        top: 0px;
                                        right: 50%;
                                        margin-top:15%;
                                        border-radius: 100%;
                                        background-color: red;
                                        color: white;" class="badge cartBN"><?php echo sizeof($_SESSION["idCart"]) ?></span>
                                <?php endif; ?>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
                <svg width="100%" height="10">
                    <rect width="100%" height="10" style="fill:silver;stroke-width:5;stroke:black" />
                </svg>
            </header>
        </div>
        <?php require($templateParams["nome"]); ?>
        <div class="modal fade" id="modal-login">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Effettua il Login</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body row">
                        <p class="mx-5">Prima di poter acquistare dei biglietti per gli eventi effettua il login nel sito.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                        <button type="button" class="btn btn-primary buy" onclick="location.href='login.php'">Vai al login</button>
                    </div>
                </div>
            </div>
        </div>
       </body>
</html>