<?php if(empty($templateParams["eventi"])): ?>
    <h3 class="text-center mt-5">Nessun evento trovato</h3>
<?php else: ?>
    <?php foreach($templateParams["eventi"] as $evento): ?>
        <article class="border bg-white my-4 pb-2 px-2" style="border-radius: 8px;">
            <header class="mt-2 mb-3">
                <a href="evento.php?id=<?php echo $evento["idEvento"] ?>"><h2 style="margin-left: 3%;"><?php echo $evento["titolo"] ?></h2></a>
            </header>
            <section class="row">
                <div class="col-md-4 col-12 text-center container-fluid">
                    <img class="img-fluid" src="<?php echo $evento["immagine"] ?>" alt="Immagine evento">
                </div>
                <div class="col-md-5 col-12">
                    <p><strong>Data:</strong> <?php echo $evento["data"] ?><br/>
                    <strong>Città:</strong> <?php echo $evento["città"] ?><br/>
                    <strong>Ora:</strong> <?php echo substr($evento["ora"], 0, 5) ?><br/>
                    <strong>Posti disponibili:</strong> <?php echo diff($evento["numeroPosti"], $dbh->getSoldTicketById($evento["idEvento"])[0]["count"])?><br/> <!-- Qua andrebbero calcolati i posti disponobili -->
                    <strong>Descrizione:</strong> <?php echo $evento["descrizione"] ?><br/>
                    </p>
                </div>
                <div class="col-md-3 col-12 pt-3 row">
                    <p class="col-sm-8 col-12 col-md-12"><strong>Prezzo:</strong> <?php echo $evento["prezzo"] ?>€</p>
                    <?php if(diff($evento["numeroPosti"], $dbh->getSoldTicketById($evento["idEvento"])[0]["count"]) > 0): ?>
                        <div class="col-sm-4 col-12 col-md-12">
                            <button  id='<?php echo $evento["idEvento"] ?>' type="button" class="cartButton btn btn-primary <?php if(inCart($evento["idEvento"])) { echo "add" ;} ?>">Aggiungi al carrello <span class="fa fa-cart-plus"></span></button>
                    </div>
                    <?php else: ?>
                        <div class="col-sm-4 col-12 col-md-12">
                            <p class="font-weight-light font-italic">Posti esauriti</p>
                        </div>
                    <?php endif ?>
                </div>
            </section>
        </article>     
    <?php endforeach; ?>
<?php endif ?>