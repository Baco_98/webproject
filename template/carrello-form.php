<div class="container-fluid">
    <h2 class="h1 text-center my-4">Carrello</h2>
    <div class="row">
        <div class="col-md-1"></div>
        <div id="body" class="col-12 col-md-10">
            <?php if(empty($templateParams["eventi"])): ?>
                <h3 class="text-center mt-5">Nessun evento nel carrello</h3>
            <?php else: ?>
                <?php foreach($templateParams["eventi"] as $evento): ?>
                    <?php $postiDisp = diff($evento["NumeroPosti"], $dbh->getSoldTicketById($evento["idEvento"])[0]["count"])?>
                    <article class="border bg-white my-4 pb-2 px-2" style="border-radius: 8px;">
                        <header class="mt-2 mb-3">
                            <a href="evento.php?id=<?php echo $evento["idEvento"] ?>"><h2 style="margin-left: 3%;"><?php echo $evento["Titolo"] ?></h2></a>
                        </header>
                        <section class="row">
                            <div class="col-md-6 col-12">
                                <p><strong>Data:</strong> <?php echo $evento["Data"] ?><br/>
                                <strong>Città:</strong> <?php echo $evento["Città"] ?><br/>
                                <strong>Ora:</strong> <?php echo substr($evento["Ora"], 0, 5) ?><br/>
                                <strong>Posti disponibili:</strong> <?php echo $postiDisp?><br/> <!-- Qua andrebbero calcolati i posti disponobili -->
                                <strong>Descrizione:</strong> <?php echo $evento["Descrizione"] ?><br/> <!-- Qua la descrizione va tagliata -->
                                </p>
                            </div>
                            <div class="col-md-6 col-12 row">
                                <div class="col-12 row">
                                    <label class="col-6"><strong>Prezzo:</strong></label>
                                    <input onkeydown="return false" class="col-6 prezzo bg-white" style='height:60%' disabled value='<?php echo $evento["Prezzo"] ?>' type="number"></input>
                                </div>
                                <div class="col-12 row">
                                    <label class="col-6"><strong>Biglietti:</strong></label>
                                    <input class="col-6 biglietti" min="1" max="<?php echo $postiDisp ?>" style='height:60%' value='1' type="number" onkeydown="return event.keyCode !== 69 && event.keyCode !== 101" onkeypress="return event.charCode != 44 && event.charCode != 45 && event.charCode != 46"></input>
                                <div class="col-12">
                            </div>
                        </section>
                        <footer class="col-12 text-right">
                            <button id='<?php echo $evento["idEvento"] ?>' type="button" class="btn btn-secondary trash">Rimuovi <span class="fa fa-trash"></span></button>
                        </footer>
                    </article>  
                <?php endforeach; ?>
                <div class="row mr-0 mb-2">
                    <p class='col-sm-8 col-md-10 col-8 text-right mb-1 pt-1' style="font-size: 120%"><strong>Prezzo totale:</strong> <span class="pt"></span> <span class="fa fa-euro"></span></p>
                    <button type="button" class="col-sm-4 col-md-2 col-4 btn btn-primary conf" data-toggle="modal" data-target="#modal">Acquista</span></button>
                </div>

            <?php endif ?>
        </div>
        <div class="col-md-1"></div>
        <div class="modal fade" id="modal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Confermi l'acquisto dei seguenti eventi?</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body row">
                        <?php $i = 0 ?>
                        <?php foreach($templateParams["eventi"] as $evento): ?>
                            <h6 class="col-6"><?php echo $evento["Titolo"] ?></h6>
                            <p class="col-6">Biglietti: <span id="bi-conf-<?php echo $i ?>"></span></p>
                            <?php $i++ ?>
                        <?php endforeach; ?>
                        <p class="text-left ml-2"><strong>Prezzo totale:</strong> <span class="pt"></span> <span class="fa fa-euro"></span></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                        <button type="button" class="btn btn-info buy" data-dismiss="modal">Conferma acquisto</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>