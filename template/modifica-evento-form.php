<div class="container-fluid">
    <div class="row pt-3">
        <div class="col-1 col-md-1"></div>
        <div class="col-11 col-md-10 row">
            <form class="row" action="#" method="POST" enctype="multipart/form-data">
                <div class="col-12 text-center mt-2 mb-3">
                    <h1>Modifica evento</h1>
                </div>
                <?php if(isset($templateParams["erroreremodifica"])): ?>
                    <div class="col-12 text-danger">
                        <p class="h5 py-2 mb-2 text-center"><?php echo $templateParams["erroreremodifica"]; ?></p>
                    </div>
                <?php endif; ?>
                <?php if(isset($templateParams["modifica"])): ?>
                    <div class="col-12 text-success">
                        <p class="h5 py-3 text-center"><?php echo $templateParams["modifica"]; ?></p>
                    </div>
                <?php endif; ?>
                <div class="form-group col-12 col-md-6 mt-4">
                    <div class="text-center" >
                        <img src="<?php echo $evento["Immagine"] ?>" alt="user image" style="width:250px;height:250px;" id="img">
                    </div>
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="custom-file pb-5 mt-3 col-8">
                            <input type="file" class="custom-file-input" id="img-loader" accept="image/*" name="img-loader">
                            <label class="custom-file-label" for="img-loader">Cambia immagine</label>
                        </div>
                        <div class="col-2"></div>
                    </div>
                    <script>
                    $('#img-loader').change( function(event) {
                        var tmppath = URL.createObjectURL(event.target.files[0]);
                        $("#img").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
                        var fileName = $(this).val().split("\\").pop();
                        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                    });
                    </script>
                </div>
                <div class="form-group col-12 col-md-6 row pt-4">
                    <div class="col-12 row my-2">
                        <label class="col-4" for="titolo" name="titolo">Titolo</label>
                        <input type="text" class="col-8" value="<?php echo $evento["Titolo"] ?>" maxlength="50" name="titolo" id="titolo" placeholder="Inserisci il titolo">
                    </div>
                    <div class="col-12 row my-2">
                        <label class="col-4" for="citta">Città</label>
                        <input type="text" class="col-8" value="<?php echo $evento["Città"] ?>" name="citta" maxlength="30" id="citta" placeholder="Inserisci la città">
                    </div>
                    <div class="col-12 row my-2">
                        <label class="col-4" for="indirizzo">Indirizzo</label>
                        <input type="text" class="col-8" value="<?php echo $evento["Indirizzo"] ?>" name="indirizzo" maxlength="50" id="indirizzo" placeholder="Inserisci l'indirizzo">
                    </div>
                    <div class="col-12 row my-2">
                        <label class="col-4" for="data">Data</label>
                        <input type="date" class="col-8" value="<?php echo $evento["Data"] ?>" name="data" id="data" min="<?php echo date("Y-m-d"); ?>">
                        <style>
                            input[type=date]::-webkit-inner-spin-button, input[type=date]::-webkit-outer-spin-button {
                                -webkit-appearance: none;
                            }
                        </style>
                    </div>
                    <div class="col-12 row my-2">
                        <label class="col-4" for="ora">Ora</label>
                        <input type="time" class="col-8" value="<?php echo substr($evento["Ora"], 0, 5) ?>" name="ora" id="ora">
                    </div>
                    <div class="col-12 row my-2">
                        <label class="col-4" for="categoria"> Categoria</label>
                        <select class=col-8 id="categoria" name="categoria">
                            <?php
                                $cat = $dbh->getCategories();
                                foreach ($cat as $value) {
                                    $active = "";
                                    if($value['idCategoria'] == $evento['idCategoria']){
                                        $active = "selected";
                                    }
                                    echo '<option value="' . $value['idCategoria'] . '"' . $active . '>' . $value['nome'] . '</option> ';
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group col-12 col-md-6 mt-4">
                    <label class="col-12 pt-2" for="descrizione">Descirizione:</label>
                    <textarea class="col-12 pt-3 mt-2" name="descrizione" id="descrizione" style="resize:none;" maxlength="100" placeholder="Inserisci una breve descrizione dell'evento"><?php echo $evento["Descrizione"] ?></textarea>   
                </div>
                <div class="form-group col-12 col-md-6 mt-4">
                    <div class="col-12 my-4 row">
                        <div class="col-md-1"></div>
                        <label class="col-4" for="numPosti">Numero posti:</label>
                        <input type="number" class="col-8 col-md-5" value="<?php echo $evento["NumeroPosti"] ?>" name="posti" id="numPosti" min="1"  onkeydown="return event.keyCode !== 69 && event.keyCode !== 101" onkeypress="return event.charCode != 44 && event.charCode != 45 && event.charCode != 46">
                        <div class="col-md-1"></div>
                    </div>
                    <div class="col-12 my-4 row">
                        <div class="col-md-1"></div>
                        <label class="col-4" for="prezzo">Prezzo biglietto:</label>
                        <input type="number" class="col-8 col-md-5" value="<?php echo $evento["Prezzo"] ?>" name="prezzo" id="prezzo" min="0.01" onkeydown="return event.keyCode !== 69 && event.keyCode !== 101" onkeypress="return event.charCode != 46 && event.charCode != 45" step="0.01">
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="form-group text-center col-12 mt-4">
                    <button type="submit" class="btn btn-primary">Modifica evento</button>
                </div>
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>