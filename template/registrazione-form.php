<div class="container-fluid">
    <div>
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 col-12 text-center">
                <h1 class="pt-4">Registrati!</h1>
                <p class="mt-4 mx-1 h4">Entra anche tu a far parte del magico mondo di EEVEEnts, bastano pochi minuti!</p>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <?php if(isset($templateParams["erroreregistrazione"])): ?>
            <p class="h5 text-danger py-3 text-center"><?php echo $templateParams["erroreregistrazione"]; ?></p>
        <?php endif; ?>
        <?php if(isset($templateParams["registrazione"])): ?>
            <p class="h5 text-success py-3 text-center"><?php echo $templateParams["registrazione"]; ?></p>
        <?php endif; ?>
        <form action="#" method="POST" enctype="multipart/form-data">
            <div class="col-12 text-center mb-4 mt-5">
                <img src="Immagini profilo/eeveeprofilo.png" alt="user image" style="width:200px;height:200px;" id="img">
            </div>
            <div class="row">
                <div class="col-2 col-md-4"></div>
                <div class="custom-file pb-5 col-8 col-md-4">
                    <input type="file" class="custom-file-input" id="img-loader" accept="image/*" name="img-loader">
                    <label class="custom-file-label" for="img-loader">Cambia immagine</label>
                </div>
                <div class="col-2 col-md-4"></div>
            </div>
            <script>
            $('#img-loader').change( function(event) {
                var tmppath = URL.createObjectURL(event.target.files[0]);
                $("#img").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
                var fileName = $(this).val().split("\\").pop();
                $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
            });
            </script>
            <div class="form-row align-items-center">
                <div class="col-sm-2"></div>
                <div class="col-sm-4 col-12 pt-4 text-center">
                    <legend class="h4">Registrati come:</legend>
                </div>
                <div class="form-check col-sm-4 col-12 pt-4">
                    <fieldset>
                    <ul class="nav nav-pills">
                        <li class="nav-item col-6 custom-control custom-radio text-center">
                            <input type="radio" class="custom-control-input" id="cliente" name="accesso" value="cliente" checked>
                            <label class="custom-control-label" for="cliente">Cliente</label> 
                        </li>
                        <li class="nav-item col-6 custom-control custom-radio text-center">
                            <input type="radio" class="custom-control-input" id="organizzatore" name="accesso" value="organizzatore">
                            <label class="custom-control-label" for="organizzatore">Organizzatore</label>
                        </li>
                    </ul>
                    </fieldset>
                </div>
            </div>
            <div class="col-md-2 col-1"></div>
            <div class="form-group col-12 row pt-4 align-items-center">
                <div class="col-md-2"></div>
                <label for="nome"  class="col-md-4 col-6 h4 text-center">Nome</label>
                <input type="text" class="form-control col-md-4 col-5" maxlength="30" id="nome" placeholder="Inserisci Nome" name="nome">
            </div>
            <div class="form-group col-12 row py-1 align-items-center">
                <div class="col-md-2"></div>
                <label for="cognome"  class="col-md-4 col-6 h4 text-center">Cognome</label>
                <input type="text" class="form-control col-md-4 col-5" maxlength="30" id="cognome" placeholder="Inserisci Cognome" name="cognome">
            </div>
            <div class="form-group col-12 row py-1 align-items-center">
                <div class="col-md-2"></div>
                <label for="email"  class="col-md-4 col-6 h4 text-center">Email</label>
                <input type="email" class="form-control col-md-4 col-5" maxlength="50" id="email" placeholder="Inserisci Email" name="email">
            </div>
            <div class="form-group col-12 row py-1 align-items-center">
                <div class="col-md-2"></div>
                <label for="password"  class="col-md-4 col-6 h4 text-center">Password</label>
                <input type="password" class="form-control col-md-4 col-5" maxlength="30" id="password" placeholder="Inserisci Password" name="password">
            </div>
            <div class="form-group col-12 row py-1 align-items-center">
                <div class="col-md-2"></div>
                <label for="conferma"  class="col-md-4 col-6 h4 text-center">Conferma Password</label>
                <input type="password" class="form-control col-md-4 col-5" maxlength="30" id="conferma" placeholder="Inserisci Password" name="conferma">
            </div>
            <div class="form-group text-center col-12">
                <button type="submit" class="btn btn-primary">Registrati</button>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-12">
            <label>Hai già un account? <a id="linkregistrati"href="login.php">Accedi</a></label>
        </div>
    </div>
</div>