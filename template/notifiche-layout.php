<?php if(empty($lette) && empty($nonlette)): ?>
    <h3 class="text-center mt-5 h1">Nessuna notifica</h3>
<?php else: ?>
    <div class="container-fluid">
        <?php if(!empty($nonlette)): ?>

            <div class="row">
                <div class="col-md-3"></div>
                <h3 class="text-center my-4 h1 col-md-6 col-12 text-success">Nuove notifiche!</h3>
                <div class="col-12 col-md-3 col-lg-2 text-center mt-md-2">
                    <button type="button" class="btn btn-danger mb-2 mb-md-0 my-md-4" onclick="location.href='cancella-notifiche.php?elimina=1'">Elimina notifiche</button>
                </div>
                <div class="col-lg-1"></div>
            </div>

            <?php foreach($nonlette as $no): ?>
            <div class="row text-center">
                <div class="col-md-2 col-1"></div>
                <article class="border bg-white my-2 pb-2 col-md-8 col-10" style="border-radius: 8px;">
                    <header class="mt-2 mb-3">
                    <h4><?php echo $no["titolo"] . " - " . $no["data"]; ?></h4>
                    </header>
                    <p class="h5"><?php echo $no["testo"]; ?></p>
                    <?php
                        if(!empty($dbh->getEventDateById($no["idevento"]))){
                            echo "<a href='evento.php?id=" . $no["idevento"] . "'>Vai all'evento</a>";
                        }
                        else{
                            if(empty($no["idevento"])){
                                echo '<i style="color:grey;">Evento annullato</i>';
                            }
                            else{
                                echo '<i style="color:grey;">Evento terminato</i>';
                            }
                        }
                    ?>
                </article>
                <div class="col-md-2 col-1"></div>
            </div>
            <?php endforeach; ?>
        <?php endif ?>
        <?php if(!empty($lette)): ?>


            <?php if(empty($nonlette)): ?>
                <div class="row">
                    <div class="col-md-3"></div>
                    <h3 class="text-center my-4 h2 col-md-6 col-12">Notifiche già visualizzate</h3>
                    <div class="col-12 col-md-3 col-lg-2 mt-md-1 text-center">
                        <button type="button" class="btn btn-danger mb-2 mb-md-0 my-md-4" onclick="location.href='cancella-notifiche.php?elimina=1'">Elimina notifiche</button>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            <?php else: ?>
                <h3 class="text-center my-4 h2">Notifiche già visualizzate</h3>
            <?php endif ?>


            <?php foreach($lette as $si): ?>
            <div class="row text-center">
                <div class="col-md-2 col-1"></div>
                <article class="border bg-white my-2 pb-2 col-md-8 col-10" style="border-radius: 8px;">
                    <header class="mt-2 mb-3">
                    <h4><?php echo $si["titolo"] . " - " . $si["data"]; ?></h4>
                    </header>
                    <p class="h5"><?php echo $si["testo"]; ?></p>
                    <?php
                        if(!empty($dbh->getEventDateById($si["idevento"]))){
                            echo "<a href='evento.php?id=" . $si["idevento"] . "'>Vai all'evento</a>";
                        }
                        else{
                            echo '<i style="color:grey;">Evento terminato</i>';
                        }
                    ?>
                </article>
                <div class="col-md-2 col-1"></div>
            </div>
            <?php endforeach; ?>
        <?php endif ?>
    </div>
    <?php $dbh->notificationsRead($_SESSION["idutente"]); ?>
<?php endif ?>