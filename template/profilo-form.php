<div class="container-fluid">
    <h1 class="text-center mt-3 pb-3">Il tuo profilo</h1>
    <?php if(isset($templateParams["erroremodifica"])): ?>
    <p class="h5 text-danger pb-3 text-center"><?php echo $templateParams["erroremodifica"]; ?></p>
    <?php endif; ?>
    <?php if(isset($templateParams["modifica"])): ?>
    <p class="h5 text-success pb-3 text-center"><?php echo $templateParams["modifica"]; ?></p>
    <?php endif; ?>
    <div class="row">
        <div class="col-1 col-md-3"></div>
        <div class="col-10 col-md-6">
            <form  action="#" method="POST" enctype="multipart/form-data">
                <div class="col-12 text-center mb-4">
                    <img src="<?php echo $info["immagine"] ; ?>" alt="user image" style="width:200px;height:200px;" id="img">
                </div>
                <div class="row">
                    <div class="col-2"></div>
                    <div class="custom-file pb-5 col-8">
                        <input type="file" class="custom-file-input" id="img-loader" accept="image/*" name="img-loader">
                        <label class="custom-file-label" for="img-loader">Cambia immagine</label>
                    </div>
                    <div class="col-2"></div>
                </div>
                <script>
                $('#img-loader').change( function(event) {
                    var tmppath = URL.createObjectURL(event.target.files[0]);
                    $("#img").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
                    var fileName = $(this).val().split("\\").pop();
                    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                });
                </script>
                <div class="col-12 text-center py-2 pt-5">
                    <p style="font-size:150%">Sei registrato come "<?php echo $_SESSION["tipo"] ?>"</p>
                </div>
                <div class="form-group row align-items-center pr-3">
                    <label for="nome" class="col-4">Nome</label>
                    <input type="text" class="form-control col-8" id="nome" name="nome" value="<?php echo $info["nome"] ?>">
                </div>
                <div class="form-group row align-items-center pr-3">
                    <label for="cognome" class="col-4">Cognome</label>
                    <input type="text" class="form-control col-8" id="cognome" name="cognome" value="<?php echo $info["cognome"] ?>">
                </div>
                <div class="form-group row align-items-center pr-3">
                    <label for="email" class="col-4">Email</label>
                    <input type="email" class="form-control col-8" id="email" name="email" value="<?php echo $info["email"] ?>">
                </div>
                <div class="form-group row pr-3">
                    <div class="form-check col-4 pt-1">
                        <input class="form-check-input" type="checkbox" id="modifica" name="modifica">
                        <label class="form-check-label" for="modifica">Modifica password</label>
                    </div>
                    <label for="password" class="col-md-3 col-4 pt-1" style="color: darkgrey;" id="pswlabel">Nuova password</label>
                    <input type="password" class="form-control col-md-5 col-4 my-auto" id="password" name="password" disabled>
                    <script>
                        document.getElementById('modifica').onchange = function() {
                            document.getElementById('password').disabled = !this.checked;
                            if(document.getElementById('pswlabel').style.color == "black"){
                                document.getElementById('pswlabel').style.color = "darkgrey";
                            }
                            else{
                                document.getElementById('pswlabel').style.color = "black";
                            }
                        };
                    </script>
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary">Salva modifiche</button>
                </div>
            </form>
        </div>    
        <div class="col-1 col-md-3"></div>
    </div>
</div>