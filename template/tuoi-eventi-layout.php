<div class="container-fluid">
    <h2 class="h1 text-center my-4">
        <?php
            if($_SESSION["tipo"] != "amministratore"){
                echo "I tuoi eventi";
            }
            else{
                echo "Tutti gli eventi";
            }
        ?>
    </h2>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-12 col-md-10">
        <?php if(empty($templateParams["eventi"])): ?>
            <h3 class="text-center mt-5">Non hai ancora creato nessun evento. Per crearne uno <a href="crea-evento.php">clicca qui!</a></h3>
        <?php else: ?>
            <?php foreach($templateParams["eventi"] as $evento): ?>
                <article class="border bg-white my-4 pb-2 px-2" style="border-radius: 8px;">
                    <header class="mt-2 mb-3">
                        <h2 style="margin-left: 3%;"><?php echo $evento["Titolo"] ?></h2>
                    </header>
                    <section class="row">
                        <div class="col-md-4 col-12 text-center container-fluid">
                            <img class="img-fluid" src="<?php echo $evento["Immagine"] ?>" alt="Immagine evento">
                        </div>
                        <div class="col-md-5 col-12">
                            <p><strong>Data:</strong> <?php echo $evento["Data"] ?><br/>
                            <strong>Città:</strong> <?php echo $evento["Città"] ?><br/>
                            <strong>Ora:</strong> <?php echo substr($evento["Ora"], 0, 5)  ?><br/>
                            <strong>Posti disponibili:</strong> <?php echo diff($evento["NumeroPosti"], $dbh->getSoldTicketById($evento["idEvento"])[0]["count"])?><br/> <!-- Qua andrebbero calcolati i posti disponobili -->
                            <strong>Prezzo:</strong> <?php echo $evento["Prezzo"] ?>€<br/>
                            <strong>Descrizione:</strong> <?php echo $evento["Descrizione"] ?>
                            </p>
                        </div>
                        <div class="col-md-3 col-12 pt-3 row text-center">
                            <div class="col-sm-6 col-12 col-md-12 mt-2">
                                <button type="button" onclick="location.href='modifica-evento.php?id=<?php echo $evento['idEvento'] ?>'" class="btn btn-primary">Modifica evento</button>
                            </div>
                            <div class="container col-sm-6 col-12 col-md-12 mt-2">
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-<?php echo $evento['idEvento'] ?>">Annulla evento</button>
                                
                                <?php if($_SESSION["tipo"] != "amministratore" || $dbh->getTypeOfUser($evento["idUtente"])[0]["tipo"] == "amministratore"): ?>


                                    <div class="modal fade" id="modal-<?php echo $evento['idEvento'] ?>">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Sei sicuro di voler anunullare questo evento?</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>
                                                <div class="modal-body">
                                                Tutti i dati relativi all'evento verranno eliminati.<br/>
                                                Gli utenti che hanno acquistato un biglieto saranno notificati della cancellazione e saranno rimborsati.
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                                    <button type="button" class="btn btn-danger" onclick="location.href='cancella-evento.php?id=<?php echo $evento['idEvento'] ?>'">Annulla evento</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                <?php else: ?>

                                    <div class="modal fade" id="modal-<?php echo $evento['idEvento'] ?>">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Sei sicuro di voler anunullare questo evento?</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>
                                                <div class="modal-body">
                                                Tutti i dati relativi all'evento verranno eliminati.<br/>
                                                Gli utenti che hanno acquistato un biglieto saranno notificati della cancellazione e saranno rimborsati.
                                                </div>
                                                <form action="cancella-evento.php?id=<?php echo $evento['idEvento'] ?>" method="POST">
                                                    <label for="motivazione">Motivo della cancellazione</label>
                                                    <textarea style="resize: none;" name="motivazione" id="motivazione" cols="30" rows="2" maxlength="50"></textarea>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                                        <button type="submit" class="btn btn-danger">Annulla evento</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                <?php endif ?>


                            </div>
                    </section>
                </article>     
            <?php endforeach; ?>
        <?php endif ?>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>