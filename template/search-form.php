<div class="container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-12 col-md-10">
            <h3 class="text-center mt-4">Riempi i filtri che vuoi impostare per la ricerca</h3>
            <form>
                <div class="input-group mt-5 mb-5">
                    <label class="col-4 col-md-2" for="cittasearch">Città:</label>
                    <input id="cittasearch" type="text" class="form-group col-8 col-md-4" name="citta" placeholder="Città dove si tiene l'evento"
                    <?php if(isset($templateParams["citta"])){ echo 'value="'.$_GET["citta"].'"'; } ?>>
                    
                    <label class="col-4 col-md-2" for="titolosearch">Titolo:</label>                    
                    <input id="titolosearch" type="text" class="form-group col-8 col-md-4" name="titolo" placeholder="Titolo dell'evento"
                    <?php if(isset($templateParams["titolo_evento"])){ echo 'value="'.$templateParams["titolo_evento"].'"'; } ?>>
                    
                    <label class="col-4 col-md-2" for="datasearch">Data:</label>
                    <input id="datasearch" type="date" name="data" class="form-group col-8 col-md-4" min="<?php echo date("Y-m-d"); ?>">

                    <label class="col-4 col-md-2" for="categoriasearch">Categoria:</label>
                    <select id="categoriasearch" class="form-group col-8 col-md-4" name="categoria">
                        <option value='0'>Non selezionato</option>
                        <?php
                            $cat = $dbh->getCategories();
                            foreach ($cat as $value) {
                                echo '<option value="' . $value['idCategoria'] . '">' . $value['nome'] . '</option>';
                            }
                        ?>
                    </select>
                    <div class="input-group-btn text-right col-12">
                        <button class="btn btn-light" type="button" id="buttonSearch" onclick="myFunction();"><span class="fa fa-search"></span>Cerca</button>
                    </div>
                </div>
            </form>
            <div>
                <h3>Eventi trovati:</h3>
                <span id="searchResult"></span>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>

<script>
    function myFunction(){
        let c = document.getElementById("cittasearch").value;
        let t = document.getElementById("titolosearch").value;
        let d = document.getElementById("datasearch").value;
        let cat = document.getElementById("categoriasearch");
        let catvalue = cat.options[cat.selectedIndex].value;
        startSearch(c,t,d,catvalue);
    }

    $(document).ready(myFunction());
</script>