<div class="container-fluid">
    <div class="row pt-3">
        <div class="col-md-2"></div>
        <div class="col-11 col-md-8">
            <form action="#" method="POST">
                <h2>Login</h2>
                <?php if(isset($templateParams["errorelogin"])): ?>
                <p class="h5 text-danger py-3"><?php echo $templateParams["errorelogin"]; ?></p>
                <?php endif; ?>
                <fieldset>
                <legend>Accedi come:</legend>
                <ul class="form-check nav nav-pills">
                    <li class="nav-item col-sm-4 col-6 custom-control custom-radio text-center">
                        <input type="radio" class="custom-control-input" id="cliente" name="accesso" value="cliente" checked>
                        <label class="custom-control-label" for="cliente">Cliente</label> 
                    </li>
                    <li class="nav-item col-sm-4 col-6 custom-control custom-radio text-center">
                        <input type="radio" class="custom-control-input" id="organizzatore" name="accesso" value="organizzatore">
                        <label class="custom-control-label" for="organizzatore">Organizzatore</label>
                    </li>
                    <li class="nav-item col-sm-4 col-12 custom-control custom-radio text-center">
                        <input type="radio" class="custom-control-input" id="amministratore" name="accesso" value="amministratore">
                        <label class="custom-control-label" for="amministratore">Amministratore</label>
                    </li>
                </ul>
                </fieldset>
                <div class="form-group row pt-4 align-items-center">
                    <label for="email" class="col-4">Email</label>
                    <input type="email" name="email" class="form-control col-8" id="email"  placeholder="Inserisci email">
                </div>
                <div class="form-group row align-items-center">
                    <label for="password"  class="col-4">Password</label>
                    <input type="password" name="password" class="form-control col-8" id="password" placeholder="Inserisci Password">
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary">Accedi</button>
                </div>
            </form> 
            <img src="Immagini/eeveelogin.png" alt="eevee_icon" style="width:300px;">
        </div>
        <div class="col-1 col-md-2"></div>
    </div>
    <br/>
    <div class="row">
        <div class="col-12">
            <label>Non hai un account? <a id="linkregistrati" href="registrazione.php">Registrati!</a></label>
        </div>
    </div>
</div>