<?php if(empty($ids) && empty($pastIds)): ?>
    <h3 class="text-center mt-5 h1">Nessun biglietto acquistato</h3>
<?php else: ?>
    <div class="container-fluid">
        <?php if(!empty($ids)): ?>
            <h3 class="text-center my-4 h1 text-info">I tuoi biglietti<i class="fa fa-ticket ml-2"></i></h3>
            <?php foreach($tickets as $ticket): ?>
            <div class="row text-center">
            <?php
                $evento = $dbh->getTitleAndDateById($ticket[0]["idevento"])[0];
                    $plurale = "biglietto";
                    $testo = "Il codice del tuo biglietto è: ";
                    if(count($ticket) > 1){
                        $plurale = "biglietti";
                        $testo = "I codici dei tuoi biglietti sono: ";
                    }
            ?>
                <div class="col-md-2 col-1"></div>
                <article class="border bg-white my-2 pb-2 col-md-8 col-10" style="border-radius: 8px;">
                    <header class="mt-2 mb-3">
                    <h4>Hai acquistato <strong style="color:red;"><?php echo count($ticket) . " " . $plurale; ?></strong> per l'evento "<a href="evento.php?id=<?php echo $ticket[0]["idevento"]; ?>"><?php echo $evento["titolo"] . '" del ' . $evento["data"]; ?></a></h4>
                    </header>
                    <p class="h5"><?php echo $testo; foreach($ticket as $t){ echo $t["codice"] . "; "; } ?></p>
                </article>
                <div class="col-md-2 col-1"></div>
            </div>
            <?php endforeach; ?>
        <?php endif ?>
        <?php if(!empty($pastIds)): ?>
            <h3 class="text-center my-4 h2">Biglietti scaduti</h3>
            <?php foreach($pastTickets as $pastTicket): ?>
            <div class="row text-center">
            <?php
                $evento = $dbh->getTitleAndDateById($pastTicket[0]["idevento"])[0];
                $plurale = "biglietto";
                $testo = "Il codice del tuo biglietto era: ";
                if(count($pastTicket) > 1){
                    $plurale = "biglietti";
                    $testo = "I codici dei tuoi biglietti erano: ";
                }
            ?>
                <div class="col-md-2 col-1"></div>
                <article class="border bg-white my-2 pb-2 col-md-8 col-10" style="border-radius: 8px;">
                    <header class="mt-2 mb-3">
                    <h5>Avevi acquistato <strong><?php echo count($pastTicket) . " " . $plurale; ?></strong> per l'evento "<?php echo $evento["titolo"] . '" del ' . $evento["data"]; ?></h5>
                    </header>
                    <p class="h5"><?php echo $testo; foreach($pastTicket as $t){ echo $t["codice"] . "; "; } ?></p>
                </article>
                <div class="col-md-2 col-1"></div>
            </div>
            <?php endforeach; ?>
        <?php endif ?>
    </div>
<?php endif ?>