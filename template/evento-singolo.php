<article class="border bg-white my-4 mx-3 pb-2 px-2" style="border-radius: 8px;">
    <header class="mt-2 mb-3 text-center">
        <h2 class="h1"><?php echo $evento["Titolo"] ?></h2>
    </header>
    <section class="row">
        <div class="col-12 text-center">
            <img class="img-fluid" src="<?php echo $evento["Immagine"] ?>" alt="Immagine evento">
        </div>
        <div class="col-1 col-md-2"></div>
        <div class="col-10 col-md-8 mt-4">
            <div class="row">
                <p class="col-8 col-md-4"><strong>Data:</strong> <?php echo $evento["Data"] ?></p>
                <p class="col-4 col-md-3"><strong>Ora:</strong> <?php echo substr($evento["Ora"], 0, 5) ?></p>
                <div class="col-1"></div>
                <p class="col-12 col-md-4"><strong>Categoria:</strong> <?php echo $categoria ?></p>
            </div>
            <div class="row">
                <p class="col-12 col-md-8"><strong>Indirizzo:</strong> <?php echo $evento["Indirizzo"] ?>
                <p class="col-12 col-md-4"><strong>Città:</strong> <?php echo $evento["Città"] ?></p>
            </div>
            <div class="row">
                <p class="ml-3"><strong>Creato da:</strong> <?php echo $creatore["nome"] . " " . $creatore["cognome"] ?></p>
                <img class="ml-2" src="<?php echo $creatore["immagine"] ?>" alt="immagine creatore" style="height:30px;">
            </div>
            <div class="row">
                <p class="col-12"><strong>Descrizione:</strong> <?php echo $evento["Descrizione"] ?></p>
            </div>
            <div class="row">
                <p class="col-6 col-md-8"><strong>Posti disponibili:</strong> <?php echo diff($evento["NumeroPosti"], $dbh->getSoldTicketById($evento["idEvento"])[0]["count"])?></p><!-- Qua andrebbero calcolati i posti disponobili -->
                <p class="col-6 col-md-4"><strong>Prezzo:</strong> <?php echo $evento["Prezzo"] ?>€</p>
            </div>
            <?php if(diff($evento["NumeroPosti"], $dbh->getSoldTicketById($evento["idEvento"])[0]["count"]) > 0): ?>
                <form>
                    <div class="row">
                        <div class="col-12 col-md-8 mt-3"></div>
                        <div class="col-12 col-md-4 mt-3">
                            <button id='<?php echo $evento["idEvento"] ?>' type="button" class="cartButton btn btn-primary text-right <?php if(inCart($evento["idEvento"])) { echo "add" ;} ?>">Aggiungi al carrello <span class="fa fa-cart-plus"></span></button>
                        </div>
                    </div>
                </form>
            <?php else: ?>
                <p class="font-weight-light font-italic">Tutti i posti sono esauriti, non è possibile acquistare i biglietti</p>
            <?php endif ?>
        </div>
    </section>
</article>
