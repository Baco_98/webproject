<?php
require_once("bootstrap.php");

$templateParams["titolo"] = "EEVEEnts - I tuoi eventi";
$templateParams["nome"] = "tuoi-eventi-layout.php";

if($_SESSION["tipo"] == "amministratore"){
    $templateParams["eventi"] = $dbh->getAllEvent();
}
else{
    $templateParams["eventi"] = $dbh->getEventByCreator($_SESSION["idutente"]);
}

require("template/base.php");
?>