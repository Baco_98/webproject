<?php
require_once("bootstrap.php");

var_dump($_GET);
var_dump($_POST);
if(isset($_GET["id"]) && isset($_POST["motivazione"])){
    $evento = $dbh->getEventById($_GET["id"])[0];
    $ids = $dbh->getUserIdByEvent($_GET["id"]);
    $titolo = "Oh no, evento annullato!";
    $testo = "L' evento " . '"' . $evento["Titolo"] . '" al quale dovevi partecipare è stato annullato!';
    foreach ($ids as $id){
        $dbh->notifyUserCancelled($titolo,$testo,date("Y/m/d"),$id);
    }
    if($_SESSION["tipo"] == "amministratore" && $dbh->getTypeOfUser($evento["idUtente"])[0]["tipo"] != "amministratore"){
        $titolo = "Oh no, evento cancellato!";
        $testo = "Ci dispiace, il tuo evento " . '"' . $evento["Titolo"] . '" è stato eliminato dall' . "'amministratore perchè " . '"' . $_POST["motivazione"] . '".' ;
        $dbh->notifyUserCancelled($titolo,$testo,date("Y/m/d"),$evento["idUtente"]);
    }
    
    $dbh->deleteTickets($_GET["id"]);
    $dbh->deleteNotification($_GET["id"]);
    $dbh->deleteEvent($_GET["id"]);
    header("Location: tuoi-eventi.php");
    exit();
}
elseif(isset($_GET["id"])){
    $evento = $dbh->getEventById($_GET["id"])[0];
    $ids = $dbh->getUserIdByEvent($_GET["id"]);
    $titolo = "Oh no, evento annullato!";
    $testo = "L' evento " . '"' . $evento["Titolo"] . '" al quale dovevi partecipare è stato annullato!';
    foreach ($ids as $id){
        $dbh->notifyUserCancelled($titolo,$testo,date("Y/m/d"),$id);
    }
    if($_SESSION["tipo"] == "amministratore" && $dbh->getTypeOfUser($evento["idUtente"])[0]["tipo"] != "amministratore"){
        $titolo = "Oh no, evento cancellato!";
        $testo = "Ci dispiace, l'evento " . '"' . $evento["Titolo"] . '" che avevi creato è stato eliminato dall' . "'amministratore.";
        $dbh->notifyUserCancelled($titolo,$testo,date("Y/m/d"),$evento["idUtente"]);
    }
    
    $dbh->deleteTickets($_GET["id"]);
    $dbh->deleteNotification($_GET["id"]);
    $dbh->deleteEvent($_GET["id"]);
    header("Location: tuoi-eventi.php");
    exit();
}

?>