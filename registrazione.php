<?php
require_once("bootstrap.php");

if(isset($_POST["accesso"])){
    //controllo se ha inserito tutte le informazioni
    if(empty($_POST["nome"]) || empty($_POST["cognome"]) || empty($_POST["email"]) || empty($_POST["password"])  || empty($_POST["conferma"])){
        $templateParams["erroreregistrazione"] = "Errore nella registrazione. Inserire tutte le informazioni.";
    }
    else{
        $login_result = $dbh->checkEmail($_POST["email"]);
        if(count($login_result)>=1){ //controllo se l'email è gia presente nel database
            $templateParams["erroreregistrazione"] = "Errore nella registrazione. Email già utilizzata.";
        }
        else{

            if($_POST["password"] != $_POST["conferma"]){ //controllo se le password coincidono
                $templateParams["erroreregistrazione"] = "Errore nella registrazione. Le password non coincidono.";
            }
            else{
                $uploaddir = 'Immagini profilo/';
                $hash = password_hash($_POST["password"], PASSWORD_DEFAULT); //cripto la password

                if(is_uploaded_file($_FILES['img-loader']['tmp_name'])){ //controllo se è stata cambiata l'immagine di default
                    $img_tmp = $_FILES['img-loader']['tmp_name'];
                    $img_name = $_FILES['img-loader']['name'];
                    while (file_exists($uploaddir . $img_name)) { //controllo se un esiste già un file con lo stesso nome nella cartella di destinazione
                        $reverse = explode('.', strrev($img_name), 2);
                        $img_name = strrev($reverse[1]) . '1.' . strrev($reverse[0]); //aggiungo degli "1" finali se esiste già un file con lo stesso nome
                    }
                    if (!move_uploaded_file($img_tmp, $uploaddir . $img_name)) { //salvo l'immagine e controllo se si è verificato un errore
                        $templateParams["errorerecreazione"] = "Errore nel caricamento dell'immagine.";
                        $dbh->insertUser($_POST["nome"],$_POST["cognome"],$_POST["email"],$hash,$_POST["accesso"], $uploaddir . "eeveeprofilo.png");
                    }else{
                        $dbh->insertUser($_POST["nome"],$_POST["cognome"],$_POST["email"],$hash,$_POST["accesso"], $uploaddir . $img_name);
                        $templateParams["registrazione"] = "Registrazione effettuata con successo.";
                    }
                }
                else{
                    $dbh->insertUser($_POST["nome"],$_POST["cognome"],$_POST["email"],$hash,$_POST["accesso"], $uploaddir . "eeveeprofilo.png");
                    $templateParams["registrazione"] = "Registrazione effettuata con successo.";
                }

                $login_result = $dbh->checkLogin($_POST["email"], $_POST["accesso"]);
                registerLoggedUser($login_result[0]); //aggiungo le informazioni nella variabile $_POST
                header("Location: index.php"); //invio alla homepage
                exit();
            }
        }
    }
}

$templateParams["titolo"] = "EEVEEnts - Registrazione";
$templateParams["nome"] = "registrazione-form.php";

require("template/base.php");
?>