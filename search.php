<?php
require_once("bootstrap.php");

$eventi=array();

if(isset($_GET["citta"]) && $_GET["citta"]!=""){
    $templateParams["citta"]=$_GET["citta"];

    if(empty($eventiCitta)){
        $eventi = $dbh->getEventsByCitta($templateParams["citta"]);
    } else {
        array_intersect($eventi, $dbh->getEventsByCitta($templateParams["citta"]));
    }
}


$templateParams["js"] = array("js/search.js", "js/eventInCart.js");
$templateParams["titolo"] = "EEVEEnts";
$templateParams["nome"] = "search-form.php";
$templateParams["eventi"] = $eventi;

require("template/base.php");
?>