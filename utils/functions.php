<?php

function isUserLoggedIn(){
    return !empty($_SESSION["idutente"]);
}

function registerLoggedUser($user){
    $_SESSION["idutente"] = $user["idutente"];
    $_SESSION["nome"] = $user["nome"];
    $_SESSION["cognome"] = $user["cognome"];
    $_SESSION["email"] = $user["email"];
    $_SESSION["password"] = $user["password"];
    $_SESSION["immagine"] = $user["immagine"];
    $_SESSION["tipo"] = $user["tipo"];
    $_SESSION["idCart"] = array();
}

function updateInfo($nome,$cogn,$email,$passw,$img){
    $_SESSION["nome"] = $nome;
    $_SESSION["cognome"] = $cogn;
    $_SESSION["email"] = $email;
    $_SESSION["password"] = $passw;
    $_SESSION["immagine"] = $img;
}

function diff(int $a, int $b){
    return  $a - $b; 
} 

function inCart($id){
    return in_array($id, $_SESSION["idCart"]);
}
?>