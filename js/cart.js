$(document).ready(function(){
    $(".trash").click(function(){
        const button = $(this);
        $.getJSON("cartManagement.php?id="+button.attr("id"), function(str){
        console.log(str)});
        location.reload();
    });

    $(".biglietti").change(function(){

        if(!$(this).val()){
            $(this).val($(this).attr("min"));
        }

        if(parseFloat($(this).val()) > parseFloat($(this).attr("max"))){
            $(this).val(parseFloat($(this).attr("max")));
        }

        $(".pt").html(changePrize());
    });

    $(".buy").click(function(){
        const inputB = $(".biglietti");
        let biglietti = [];
        for (const biglietto of inputB) {
            biglietti.push(biglietto.value);
        }

        $.getJSON("acquista.php?b="+JSON.stringify(biglietti), function(str){
            $("#body").html(`
            <div class="alert alert-success text-center">
                <strong>Successo!</strong> Acquisto completato con successo.
            </div>
            <div class="row">
                <div class="col-5 col-sm-7 col-md-10"></div>
                <button class="btn btn-primary col-6 col-sm-4 col-md-2" onclick="location.href='index.php'">Torna alla home</button>
            </div>
            `);
        });
        updateBadge(-biglietti.length);
    });
    
    $(".pt").html(changePrize());

    $(".conf").click(function(){
        changeModal();
    });

});

function changeModal(){

    const inputB = $(".biglietti");
    let i=0;
    for (const biglietto of inputB) {
        $("#bi-conf-"+i).html(biglietto.value);
        i++;
    }
}

function changePrize(){
    const inputP = $(".prezzo");
    const inputB = $(".biglietti");

    let prezzi = [];
    let biglietti = [];

    for (const prezzo of inputP) {
        prezzi.push(prezzo.value);
    }

    for (const biglietto of inputB) {
        biglietti.push(biglietto.value);
    }

    let prize = 0.0;

    for(i=0; i<prezzi.length; i++){

        prize += (parseFloat(prezzi[i]) * parseFloat(biglietti[i]));
    }   

    return prize.toFixed(2);
}