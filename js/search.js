function showEvents(events){
    let result="";
    let cartInfo = "";

    for(let i=0; i < events.length; i++){

        if(events[i]["NumeroPosti"] <= 0){
            cartInfo = `<p class="font-weight-light font-italic">Posti esauriti</p>`;
        } else if(!events[i]["userLogged"]){
            cartInfo = `<button id="${events[i]["idEvento"]}" data-toggle="modal" data-target="#modal-login" type="button" class="btn btn-primary ${events[i]["inCart"]}">Aggiungi al carrello <span class="fa fa-cart-plus"></span></button>`;
        } else {
            cartInfo = `<button id="${events[i]["idEvento"]}" type="button" class="cartButton btn btn-primary ${events[i]["inCart"]}">Aggiungi al carrello <span class="fa fa-cart-plus"></span></button>`;
        }

        let event = `
            <article class="border bg-white my-4 pb-2 px-2" style="border-radius: 8px;">
            <header class="mt-2 mb-3">
                <a href="evento.php?id=${events[i]["idEvento"]}"><h2 style="margin-left: 3%;">${events[i]["Titolo"]}</h2></a>
            </header>
            <section class="row">
                <div class="col-md-4 col-12 text-center">
                    <img class="img-fluid" src="${events[i]["Immagine"]}" alt="Immagine evento" style="max-height:250px;max-width:300px;">
                </div>
                <div class="col-md-5 col-12">
                    <p><strong>Data:</strong> ${events[i]["Data"]}<br/>
                    <strong>Città:</strong> ${events[i]["Città"]}<br/>                     
                    <strong>Ora:</strong> ${events[i]["Ora"].substr(0, 5)}<br/> 
                    <strong>Posti disponibili:</strong> ${events[i]["NumeroPosti"]}<br/> <!-- Qua andrebbero calcolati i posti disponobili -->
                    <strong>Descrizione:</strong> ${events[i]["Descrizione"]}<br/>
                    </p>
                </div>
                <div class="col-md-3 col-12 pt-3 row">
                    <p class="col-sm-8 col-12 col-md-12"><strong>Prezzo:</strong> ${events[i]["Prezzo"]}€</p>
                    <div class="col-sm-4 col-12 col-md-12">
                        ${cartInfo}
                    </div>
                </div>
            </section>
        </article> 
        
        `;
        result += event;
    }

    if(events.length == 0){
        result = '<h3 class="text-center mt-5">Nessun evento trovato</h3>'
    }

    return result;

}

function startSearch(citta, titolo, giorno, cat) {
    const par = "?d="+giorno+"&c="+citta+"&t="+titolo+"&cat="+cat;
    const link = "research.php"+par;
    $.getJSON(link, function(str){
        let eventi=showEvents(str);
        document.getElementById("searchResult").innerHTML = eventi;
        updatecartButton();
    });
}