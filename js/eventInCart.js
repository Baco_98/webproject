$(document).ready(function(){

    updatecartButton();

});

function updatecartButton(){

    $(".cartButton").click(function(){
        button = $(this);
        if(!x){
            button.attr("data-toggle", "modal");
            button.attr("data-target", "#modal-login");
        } else {
            
            if(button.hasClass("add")){
                button.html("Aggiungi al carrello <span class='fa fa-cart-plus'>");
                updateBadge(-1);
            } else {
                button.html("Nel carrello <span class='fa fa-check'>");
                updateBadge(+1);
            }
            
            button.toggleClass("add");

            $.getJSON("cartManagement.php?id="+button.attr("id"), function(str){
            });
        }
    });

    $("[class~='add']").html("Nel carrello <span class='fa fa-check'>");

    updateBadge();
}
