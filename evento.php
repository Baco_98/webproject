<?php
require_once("bootstrap.php");

$templateParams["titolo"] = "EEVEEnts - Evento";
$templateParams["nome"] = "evento-singolo.php";
$templateParams["js"] = array("js/eventInCart.js");

$evento = $dbh->getEventById($_GET['id'])[0];
$categoria = $dbh->getCategoryById($evento["idCategoria"])[0]["nome"];
$creatore = $dbh->getCreator($evento["idUtente"])[0];
require("template/base.php");
?>