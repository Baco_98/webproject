<?php
require_once("bootstrap.php");

$eventi = array();

foreach($_SESSION["idCart"] as $id){
    $eventi = array_merge($eventi, $dbh->getEventById($id));
}


$templateParams["titolo"] = "EEVEEnts - Carrello";
$templateParams["nome"] = "carrello-form.php";
$templateParams["eventi"] = $eventi;
$templateParams["js"] = array("js/cart.js");

require("template/base.php");
?>