<?php

    require_once "bootstrap.php";

    $result="";

    if(isset($_GET["b"])){

        $biglietti = json_decode($_GET["b"]);

        for($t=0; $t<sizeof($_SESSION["idCart"]); $t++){
            for($i=0; $i<$biglietti[$t]; $i++){

                
                $codice = $dbh->getMaxCodeTicketForEvent($_SESSION["idCart"][$t])[0]["max"];
                
                if($codice==null){
                    $codice = 1; 
                } else {
                    $codice += 1;
                }

                $dbh->insertTicket($_SESSION["idCart"][$t], $codice, $_SESSION["idutente"]);
            }

            if($biglietti[$t] > 1){
                $plurale = "biglietti";
            }
            else{
                $plurale = "biglietto";
            }
            $ev = $dbh->getTitleAndDateById($_SESSION["idCart"][$t])[0];
            $titolo = "Acquisto completato!";
            $testo = "Hai acquistato " . $biglietti[$t] . " " . $plurale . " per l'evento " . '"' . $ev["titolo"] . '"';
            $dbh->notifyUser($titolo,$testo,date("Y-m-d"),$_SESSION["idutente"],$_SESSION["idCart"][$t]);
            if($ev["data"] == date("Y-m-d")){
                $titolo = "Ci siamo quasi!";
                $testo = "Non scordarti di partecipare all'evento " . '"' . $ev["titolo"] . '" che si terrà oggi. Ti aspettiamo!';
                $dbh->notifyUser($titolo,$testo,date('Y-m-d', strtotime($ev["data"])),$_SESSION["idutente"],$_SESSION["idCart"][$t]);
            }
            else{
                $titolo = "Ci siamo quasi!";
                $testo = "Non scordarti di partecipare all'evento " . '"' . $ev["titolo"] . '" che si terrà domani. Ti aspettiamo!';
                $dbh->notifyUser($titolo,$testo,date('Y-m-d', strtotime($ev["data"] .' -1 day')),$_SESSION["idutente"],$_SESSION["idCart"][$t]);
            }
            if(diff($ev["numeroPosti"], $dbh->getSoldTicketById($_SESSION["idCart"][$t])[0]["count"]) == 0){
                $titolo = "Hai spaccato!";
                $testo = "L'evento " . '"' . $ev["titolo"] . '" che hai creato è andato sold out! Grande!';
                $dbh->notifyUser($titolo,$testo,date("Y/m/d"),$ev["idUtente"],$_SESSION["idCart"][$t]);
            }
        }
    }

    $_SESSION["idCart"] = array();

    echo json_encode($result);


?>