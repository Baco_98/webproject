<?php
require_once 'bootstrap.php';

if(isset($_POST["categoria"])){
    //controllo se ha inserito tutte le informazioni
    if(empty($_POST["titolo"]) || empty($_POST["descrizione"]) || empty($_POST["data"]) || empty($_POST["ora"]) || empty($_POST["posti"])  || empty($_POST["prezzo"])  || empty($_POST["citta"])  || empty($_POST["indirizzo"])){
        $templateParams["errorerecreazione"] = "Errore nella creazione dell'evento. Inserire tutte le informazioni.";
    }
    else{
        $place = $dbh->checkPlace($_POST["citta"],$_POST["indirizzo"]);
        if (empty($place[0])){ //controllo se città e indirizzo sono già presenti nel database
            $dbh->insertPlace($_POST["citta"],$_POST["indirizzo"]); //se non ci sono le inserisco
            $place = $dbh->checkPlace($_POST["citta"],$_POST["indirizzo"])[0]["idluogo"];
        }
        else{
            $place = $place[0]["idluogo"];
        }
        
        $uploaddir = 'Immagini eventi/';

        if(is_uploaded_file($_FILES['img-loader']['tmp_name'])){ //controllo se è stata cambiata l'immagine di default
            $img_tmp = $_FILES['img-loader']['tmp_name'];
            $img_name = $_FILES['img-loader']['name'];
            while (file_exists($uploaddir . $img_name)) { //controllo se un esiste già un file con lo stesso nome nella cartella di destinazione
                $reverse = explode('.', strrev($img_name), 2);
                $img_name = strrev($reverse[1]) . '1.' . strrev($reverse[0]); //aggiungo degli "1" finali se esiste già un file con lo stesso nome
            }
            if (!move_uploaded_file($img_tmp, $uploaddir . $img_name)) { //salvo l'immagine e controllo se si è verificato un errore
                $templateParams["errorerecreazione"] = "Errore nel caricamento dell'immagine.";
                $dbh->insertEvent($_POST["titolo"],$_POST["descrizione"],$_POST["data"],$_POST["ora"],$_POST["posti"],$_POST["prezzo"],$uploaddir . "default-event.jpg",$_SESSION["idutente"],$place,$_POST["categoria"]);
            }else{
                $dbh->insertEvent($_POST["titolo"],$_POST["descrizione"],$_POST["data"],$_POST["ora"],$_POST["posti"],$_POST["prezzo"],$uploaddir . $img_name,$_SESSION["idutente"],$place,$_POST["categoria"]);
                $templateParams["creazione"] = "Il tuo evento è stato correttamente creato.";
            }
        }
        else{
            $dbh->insertEvent($_POST["titolo"],$_POST["descrizione"],$_POST["data"],$_POST["ora"],$_POST["posti"],$_POST["prezzo"],$uploaddir . "default_event.jpg",$_SESSION["idutente"],$place,$_POST["categoria"]);
            $templateParams["creazione"] = "Il tuo evento è stato correttamente creato.";
        }
    }
}

$templateParams["titolo"] = "EEVEEnts - Crea evento";
$templateParams["nome"] = "crea-evento-form.php";

require("template/base.php");
?>