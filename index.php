<?php
require_once("bootstrap.php");

$templateParams["titolo"] = "EEVEEnts - Home";
$templateParams["nome"] = "homepage.php";
$templateParams["eventi"] = $dbh->getActualEvents();
$templateParams["js"] = array("js/eventInCart.js");

require("template/base.php");
?>