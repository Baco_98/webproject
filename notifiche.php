<?php
require_once("bootstrap.php");

$lette = $dbh->getReadNotificationByUser($_SESSION["idutente"]);
$nonlette = $dbh->getUnreadNotificationByUser($_SESSION["idutente"]);
$notifiche = $dbh->getNotificationByUser($_SESSION["idutente"]);
$templateParams["titolo"] = "EEVEEnts - Notifiche";
$templateParams["nome"] = "notifiche-layout.php";

require("template/base.php");
?>