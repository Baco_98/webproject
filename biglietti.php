<?php
require_once("bootstrap.php");

$templateParams["titolo"] = "EEVEEnts - I tuoi biglietti";
$templateParams["nome"] = "biglietti-layout.php";

$ids = $dbh->getPurchasedEventByUser($_SESSION["idutente"]);
$pastIds = $dbh->getPurchasedPastEventByUser($_SESSION["idutente"]);

$tickets = [];
foreach($ids as $id){
    $t = $dbh->getTicketsOfUserById($_SESSION["idutente"],$id["idevento"]);
    array_push($tickets, $t);
}

$pastTickets = [];
foreach($pastIds as $id){
    $t = $dbh->getTicketsOfUserById($_SESSION["idutente"],$id["idevento"]);
    array_push($pastTickets, $t);
}

require("template/base.php");
?>