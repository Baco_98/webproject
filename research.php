<?php

require_once("bootstrap.php");

function map($a){
    return reset($a);
}

$firstSearch = true;
$eventi=array();

if(isset($_GET["c"]) && $_GET["c"]!=""){
    $templateParams["c"]=$_GET["c"];

    if($firstSearch){
        $firstSearch = false;
        $eventi = array_map("map", $dbh->getEventsByCitta($templateParams["c"]));
    } else {
        $eventi = array_intersect($eventi, array_map("map", $dbh->getEventsByCitta($templateParams["c"])));
    }
}

if(isset($_GET["t"]) && $_GET["t"]!=""){
    $templateParams["t"]=$_GET["t"];

    if($firstSearch){
        $firstSearch = false;
        $eventi = array_map("map", $dbh->getEventsByTitolo($templateParams["t"]));
    } else {
        $eventi = array_intersect($eventi, array_map("map", $dbh->getEventsByTitolo($templateParams["t"])));
    }
}

if(isset($_GET["d"]) && $_GET["d"]!=""){
    $templateParams["d"]=$_GET["d"];

    if($firstSearch){
        $firstSearch = false;
        $eventi = array_map("map", $dbh->getEventsByData($templateParams["d"]));
    } else {
        $eventi = array_intersect($eventi, array_map("map", $dbh->getEventsByData($templateParams["d"])));
    }
}

if(isset($_GET["cat"]) && $_GET["cat"]!=0){
    $templateParams["cat"]=$_GET["cat"];

    if($firstSearch){
        $firstSearch = false;
        $eventi = array_map("map", $dbh->getEventsByCategoria($templateParams["cat"]));
    } else {
        $eventi = array_intersect($eventi, array_map("map", $dbh->getEventsByCategoria($templateParams["cat"])));
    }
}

$eventifin=array();

foreach ($eventi as $id){
    $eventifin = array_merge($eventifin, $dbh->getEventById($id));
}

foreach($eventifin as &$ev){
    $ev["NumeroPosti"] = diff($ev["NumeroPosti"],$dbh->getSoldTicketById($ev["idEvento"])[0]["count"]);

    $ev["userLogged"] = isUserLoggedIn();

    if(isUserLoggedIn() && inCart($ev["idEvento"])){
        $ev["inCart"] = "add";
    } else {
        $ev["inCart"] = "";
    }
}

$templateParams["eventi"] = $eventifin;

header('Content-Type: application/json');
echo json_encode($eventifin);

?>