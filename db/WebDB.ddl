-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Tue Jan 14 17:58:18 2020 
-- * LUN file: C:\Users\Proprietario\Desktop\WebDB.lun 
-- * Schema: WebLogico/1-1 
-- ********************************************* 


-- Database Section
-- ________________ 

create database WebLogico;
use WebLogico;


-- Tables Section
-- _____________ 

create table Biglietto (
     idEvento int not null,
     Codice int not null,
     idUtente int not null,
     constraint IDBiglietto primary key (idEvento, Codice));

create table Categoria (
     idCategoria int not null,
     Nome varchar(20) not null,
     Descrizione varchar(100) not null,
     constraint IDCategoria primary key (idCategoria));

create table Eventi (
     idEvento int not null,
     Titolo varchar(50) not null,
     Descrizione varchar(100) not null,
     Data date not null,
     NumeroPosti int not null,
     Prezzo decimal(5,2) not null,
     idUtente int not null,
     idLuogo int not null,
     idCategoria int not null,
     constraint IDEventi primary key (idEvento));

create table Luogo (
     idLuogo int not null,
     Città varchar(30) not null,
     Indirizzo varchar(50) not null,
     NumeroCivico int not null,
     constraint IDLuogo primary key (idLuogo));

create table Utente (
     idUtente int not null,
     Email varchar(30) not null,
     Nome varchar(30) not null,
     Cognome varchar(30) not null,
     Password varchar(10) not null,
     Tipo varchar(20) not null,
     constraint IDUtente primary key (idUtente));


-- Constraints Section
-- ___________________ 

alter table Biglietto add constraint FKAcquisto
     foreign key (idUtente)
     references Utente (idUtente);

alter table Biglietto add constraint FKPer
     foreign key (idEvento)
     references Eventi (idEvento);

alter table Eventi add constraint FKCreazione
     foreign key (idUtente)
     references Utente (idUtente);

alter table Eventi add constraint FKSiSvolge
     foreign key (idLuogo)
     references Luogo (idLuogo);

alter table Eventi add constraint FKAppartiene
     foreign key (idCategoria)
     references Categoria (idCategoria);


-- Index Section
-- _____________ 

