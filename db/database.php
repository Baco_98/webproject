<?php

class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if($this->db->connect_error){
            die("Connesione fallita al db");
        }
    }

    public function checkLogin($email, $tipo){
        $stmt = $this->db->prepare("SELECT idutente,nome,cognome,email,password,immagine, tipo FROM utente WHERE email = ? AND tipo = ?");
        $stmt->bind_param("ss", $email, $tipo);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkEmail($email){
        $stmt = $this->db->prepare("SELECT idutente FROM utente WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertUser($nome,$cognome,$email,$password,$tipo,$immagine){
        $stmt = $this->db->prepare("INSERT INTO utente (email,nome,cognome,password,tipo,immagine) VALUES (?,?,?,?,?,?)");
        $stmt->bind_param("ssssss", $email,$nome,$cognome,$password,$tipo,$immagine);
        
        return $stmt->execute();
    }

    public function getUser($id){
        $stmt = $this->db->prepare("SELECT nome,cognome,email,password,immagine,tipo FROM utente WHERE idutente = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function modifyUser($id,$nome,$cognome,$email,$password,$immagine){
        $query = "UPDATE utente SET nome = ?, cognome = ?, email = ?, password = ?, immagine = ? WHERE idutente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("sssssi",$nome,$cognome,$email,$password,$immagine,$id);
        
        return $stmt->execute();
    }

    public function getCategories(){
        $stmt = $this->db->prepare("SELECT idCategoria,nome FROM categoria");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertEvent($titolo,$descr,$data,$ora,$posti,$prezzo,$immagine,$idutente,$idluogo,$idcategoria){
        $stmt = $this->db->prepare("INSERT INTO eventi (titolo,descrizione,data,ora,numeroposti,prezzo,immagine,idutente,idluogo,idcategoria) VALUES (?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param("ssssidsiii", $titolo,$descr,$data,$ora,$posti,$prezzo,$immagine,$idutente,$idluogo,$idcategoria);
        
        return $stmt->execute();
    }

    public function checkPlace($citta,$indirizzo){
        $stmt = $this->db->prepare("SELECT idluogo FROM luogo WHERE città = ? AND indirizzo = ?");
        $stmt->bind_param("ss", $citta,$indirizzo);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertPlace($citta,$indirizzo){
        $stmt = $this->db->prepare("INSERT INTO luogo (città,indirizzo) VALUES (?,?)");
        $stmt->bind_param("ss", $citta,$indirizzo);
        
        return $stmt->execute();
    }

    public function getRandomEvents($n=5){
        $stmt = $this->db->prepare("SELECT idEvento, titolo, descrizione, data, numeroPosti, prezzo, immagine, città FROM eventi, luogo WHERE eventi.idLuogo=luogo.idLuogo ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i',$n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByCitta($citta){
        $stmt = $this->db->prepare("SELECT idEvento FROM eventi, luogo WHERE eventi.idLuogo=luogo.idLuogo AND città=? AND data > CURDATE() - INTERVAL 1 DAY");
        $stmt->bind_param('s',$citta);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByTitolo($titolo){
        $stmt = $this->db->prepare("SELECT idEvento FROM eventi, luogo WHERE eventi.idLuogo=luogo.idLuogo AND titolo=? AND data > CURDATE() - INTERVAL 1 DAY");
        $stmt->bind_param('s',$titolo);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByData($data){
        $stmt = $this->db->prepare("SELECT idEvento FROM eventi, luogo WHERE eventi.idLuogo=luogo.idLuogo AND data=? AND data > CURDATE() - INTERVAL 1 DAY");
        $stmt->bind_param('s',$data);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByCategoria($cat){
        $stmt = $this->db->prepare("SELECT idEvento FROM eventi, luogo WHERE eventi.idLuogo=luogo.idLuogo AND eventi.idCategoria=? AND data > CURDATE() - INTERVAL 1 DAY");
        $stmt->bind_param('i', $cat);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getUserId($email){
        $stmt = $this->db->prepare("SELECT idUtente FROM utente WHERE email = ?");
        $stmt->bind_param('s',$email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function modifyUserNoPsw($id,$nome,$cognome,$email,$immagine){
        $query = "UPDATE utente SET nome = ?, cognome = ?, email = ?, immagine = ? WHERE idutente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ssssi",$nome,$cognome,$email,$immagine,$id);
        
        return $stmt->execute();
    }

    public function getActualEvents(){
        $stmt = $this->db->prepare("SELECT idEvento, titolo, descrizione, data, ora, numeroPosti, prezzo, immagine, città FROM eventi, luogo WHERE eventi.idLuogo=luogo.idLuogo AND data > CURDATE() - INTERVAL 1 DAY ORDER BY data ASC");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventById($id){
        $stmt = $this->db->prepare("SELECT idEvento, Titolo, Descrizione, Data, Ora, NumeroPosti, Prezzo, Immagine, idUtente, idCategoria, Città, Indirizzo FROM Eventi, Luogo WHERE Eventi.idLuogo=Luogo.idLuogo AND idEvento = ?");
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCategoryById($id){
        $stmt = $this->db->prepare("SELECT nome FROM categoria WHERE idCategoria = ?");
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCreator($id){
        $stmt = $this->db->prepare("SELECT nome,cognome,immagine FROM utente WHERE idutente = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventByCreator($id){
        $stmt = $this->db->prepare("SELECT idEvento, Titolo, Descrizione, Data, Ora, NumeroPosti, Prezzo, Immagine, idUtente, idCategoria, Città, Indirizzo FROM Eventi, Luogo WHERE Eventi.idLuogo=Luogo.idLuogo AND idUtente = ? AND data > CURDATE() - INTERVAL 1 DAY ORDER BY data ASC");
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function modifytEvent($titolo,$descr,$data,$ora,$posti,$prezzo,$immagine,$idutente,$idluogo,$idcategoria,$idevento){
        $stmt = $this->db->prepare("UPDATE eventi SET titolo=?,descrizione=?,data=?,ora=?,numeroposti=?,prezzo=?,immagine=?,idutente=?,idluogo=?,idcategoria=? WHERE idEvento = ?");
        $stmt->bind_param("ssssidsiiii", $titolo,$descr,$data,$ora,$posti,$prezzo,$immagine,$idutente,$idluogo,$idcategoria,$idevento);
        
        return $stmt->execute();
    }

    public function getAllEvent(){
        $stmt = $this->db->prepare("SELECT idEvento, Titolo, Descrizione, Data, Ora, NumeroPosti, Prezzo, Immagine, idUtente, idCategoria, Città, Indirizzo FROM Eventi, Luogo WHERE Eventi.idLuogo=Luogo.idLuogo AND data > CURDATE() - INTERVAL 1 DAY ORDER BY data ASC");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotificationByUser($id){
        $stmt = $this->db->prepare("SELECT idnotifica, titolo, testo, data, letta, idevento FROM notifica WHERE idutente = ? AND data <= CURDATE() ORDER BY data ASC");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getUnreadNotificationByUser($id){
        $stmt = $this->db->prepare("SELECT idnotifica, titolo, testo, data, letta, idevento FROM notifica WHERE idutente = ? AND letta = 0 AND data <= CURDATE() ORDER BY data ASC");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getSoldTicketById($id){ 
        $stmt = $this->db->prepare("SELECT COUNT(idEvento) as count FROM biglietto WHERE idEvento = ? ");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getReadNotificationByUser($id){
        $stmt = $this->db->prepare("SELECT idnotifica, titolo, testo, data, letta, idevento FROM notifica WHERE idutente = ? AND letta = 1 AND data <= CURDATE() AND data > CURDATE() - INTERVAL 15 DAY ORDER BY data ASC");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNumberOfNotification($id){
        $stmt = $this->db->prepare("SELECT COUNT(idnotifica) as numero FROM notifica WHERE idutente = ? AND letta = 0 AND data <= CURDATE() ORDER BY data ASC");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventDateById($id){
        $stmt = $this->db->prepare("SELECT Data FROM Eventi WHERE idevento = ? AND data >= CURDATE()");
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function notificationsRead($id){
        $stmt = $this->db->prepare("UPDATE notifica SET letta = 1 WHERE idutente = ?");
        $stmt->bind_param("i", $id);

        return $stmt->execute();
    }

    public function getUserIdByEvent($id){
        $stmt = $this->db->prepare("SELECT DISTINCT idutente FROM biglietto WHERE idevento = ?");
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function notifyUser($titolo,$testo,$data,$idutente,$idevento){
        $stmt = $this->db->prepare("INSERT INTO notifica (titolo,testo,data,letta,idutente,idevento) VALUES (?,?,?,0,?,?)");
        $stmt->bind_param('sssii',$titolo,$testo,$data,$idutente,$idevento);

        return $stmt->execute();
    }

    public function notifyUserCancelled($titolo,$testo,$data,$idutente){
        $stmt = $this->db->prepare("INSERT INTO notifica (titolo,testo,data,letta,idutente,idevento) VALUES (?,?,?,0,?,NULL)");
        $stmt->bind_param('sssi',$titolo,$testo,$data,$idutente);

        return $stmt->execute();
    }
    
    public function getPurchasedEventByUser($id){ 
        $stmt = $this->db->prepare("SELECT biglietto.idevento, data, COUNT(biglietto.idevento) as count FROM biglietto,eventi WHERE biglietto.idevento = eventi.idevento AND data >= CURDATE() AND biglietto.idutente = ? GROUP BY biglietto.idevento");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPurchasedPastEventByUser($id){ 
        $stmt = $this->db->prepare("SELECT biglietto.idevento, data, COUNT(biglietto.idevento) as count FROM biglietto,eventi WHERE biglietto.idevento = eventi.idevento AND data < CURDATE() AND biglietto.idutente = ? GROUP BY biglietto.idevento");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getTicketsOfUserById($idutente, $idevento){
        $stmt = $this->db->prepare("SELECT codice,idevento FROM biglietto WHERE biglietto.idutente = ? AND biglietto.idevento = ?");
        $stmt->bind_param('ii',$idutente, $idevento);$stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getTitleAndDateById($id){
        $stmt = $this->db->prepare("SELECT titolo,data,numeroPosti,idUtente FROM Eventi WHERE idEvento = ?");
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function insertTicket($idevento, $codice, $idUtente){
        $stmt = $this->db->prepare("INSERT INTO biglietto (idEvento,Codice,idUtente) VALUES (?,?,?)");
        $stmt->bind_param("iii", $idevento, $codice, $idUtente);
        
        return $stmt->execute();
    }

    public function getMaxCodeTicketForEvent($id){
        $stmt = $this->db->prepare("SELECT MAX(codice) as max FROM biglietto WHERE idEvento = ?");
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);

    }

    public function deleteTickets($id){
        $stmt = $this->db->prepare("DELETE FROM biglietto WHERE idevento = ?");
        $stmt->bind_param("i", $id);

        return $stmt->execute();
    }

    public function deleteEvent($id){
        $stmt = $this->db->prepare("DELETE FROM eventi WHERE idevento = ?");
        $stmt->bind_param("i", $id);

        return $stmt->execute();
    }

    public function deleteNotification($id){
        $stmt = $this->db->prepare("DELETE FROM notifica WHERE idevento = ?");
        $stmt->bind_param("i", $id);

        return $stmt->execute();
    }

    public function getTypeOfUser($id){
        $stmt = $this->db->prepare("SELECT tipo FROM utente WHERE idutente = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function deleteReadNotificationOfUser($id){
        $stmt = $this->db->prepare("DELETE FROM notifica WHERE idutente = ? AND letta = 1");
        $stmt->bind_param("i", $id);

        return $stmt->execute();
    }
}

?>