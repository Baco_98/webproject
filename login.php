<?php
require_once 'bootstrap.php';

session_unset();
if(isset($_POST["email"]) && isset($_POST["password"])){
    $login_result = $dbh->checkLogin($_POST["email"], $_POST["accesso"]); //controllo se i dati sono corretti
    if(count($login_result)==0 || !password_verify($_POST["password"], $login_result[0]["password"])){ //controllo il risultato e la password
        $templateParams["errorelogin"] = "Email, password o tipo di accesso non corretti. Riprovare.";
    }
    else{
        registerLoggedUser($login_result[0]); //registro le informazioni in $_SESSION
        header("Location: index.php"); //invio alla homepage
        exit();
    }
}

$templateParams["titolo"] = "EEVEEnts - Login";
$templateParams["nome"] = "login-form.php";

require("template/base.php");
?>