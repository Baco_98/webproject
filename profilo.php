<?php
require_once("bootstrap.php");

$info = $dbh->getUser($_SESSION["idutente"])[0]; //prendo le informazioni dell'utente per precompilare le textbox
if(isset($_POST["nome"])){
    if(empty($_POST["nome"]) || empty($_POST["cognome"]) || empty($_POST["email"])){ //controllo se ha inserito tutte le informazioni
        $templateParams["erroremodifica"] = "Errore nella compilazione. Inserire tutte le informazioni.";
    }
    else{
        //controllo se è stato modificato qualcosa
        if((!isset($_FILES['img-loader']) || !is_uploaded_file($_FILES['img-loader']['tmp_name'])) && $info["nome"]==$_POST["nome"] && $info["cognome"]==$_POST["cognome"] && $info["email"]==$_POST["email"] && !isset($_POST["modifica"])){
            $templateParams["erroremodifica"] = "Errore nella compilazione. Modificare almeno un campo.";
        }
        else{
            $img = $_SESSION["immagine"];

            if(is_uploaded_file($_FILES['img-loader']['tmp_name'])){ //controllo se ha cambiato immagine
                $uploaddir = "Immagini profilo/";
                $img_tmp = $_FILES['img-loader']['tmp_name'];
                $img_name = $_FILES['img-loader']['name'];
                while (file_exists($uploaddir . $img_name)) { //controllo se un esiste già un file con lo stesso nome nella cartella di destinazione
                    $reverse = explode('.', strrev($img_name), 2);
                    $img_name = strrev($reverse[1]) . '1.' . strrev($reverse[0]); //aggiungo degli "1" finali se esiste già un file con lo stesso nome
                }
                if (!move_uploaded_file($img_tmp, $uploaddir . $img_name)) { //salvo l'immagine e controllo se si è verificato un errore
                    $templateParams["erroremodifica"] = "Errore nel caricamento dell'immagine.";
                }else{
                    $img = $uploaddir . $img_name;
                    $templateParams["modifica"] = "Modifica effettuata con successo";
                }
            }
            else{
                $templateParams["modifica"] = "Modifica effettuata con successo";
            }

            if (isset($_POST["modifica"])){ //controllo se ha modificato la password
                if (empty($_POST["password"])){
                    $templateParams["erroremodifica"] = "La nuova password non può essere vuota.";
                    $templateParams["modifica"] = "";
                }
                else{
                    $hash = password_hash($_POST["password"], PASSWORD_DEFAULT); //cripto la password
                    $dbh->modifyUser($_SESSION["idutente"],$_POST["nome"],$_POST["cognome"],$_POST["email"],$hash,$img);
                    $info = $dbh->getUser($_SESSION["idutente"])[0]; //aggiorno le informazioni della variabile che precompila le textbox
                    updateInfo($info["nome"],$info["cognome"],$info["email"],$info["password"],$info["immagine"]); //aggiorno le informazioni di $_SESSION      
                }
            }
            else{
                $dbh->modifyUserNoPsw($_SESSION["idutente"],$_POST["nome"],$_POST["cognome"],$_POST["email"],$img);
                $info = $dbh->getUser($_SESSION["idutente"])[0]; //aggiorno le informazioni della variabile che precompila le textbox
                updateInfo($info["nome"],$info["cognome"],$info["email"],$info["password"],$info["immagine"]); //aggiorno le informazioni di $_SESSION      
            }      
        }
    }
}

$templateParams["titolo"] = "EEVEEnts - Profilo";
$templateParams["nome"] = "profilo-form.php";

require("template/base.php");
?>