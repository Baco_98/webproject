<?php
    require_once "bootstrap.php";

    if(isset($_GET["id"])){
        $id = $_GET["id"];

        if(in_array($id, $_SESSION["idCart"])){
            unset($_SESSION["idCart"][array_search($id, $_SESSION["idCart"])]); 

            $new = array();

            foreach($_SESSION["idCart"] as $val){
                array_push($new, $val);
            }

            $_SESSION["idCart"] = $new;

        } else {
            array_push($_SESSION["idCart"], $id);
        }
    }

    echo json_encode($_SESSION["idCart"]);
?>